//import express Js
const express = require("express");
//Khai báo router
const router = express.Router();
//import user controller
const userController = require("../controllers/userController");

router.post("/users", userController.createUser);
router.get("/users", userController.getAllUser);
router.put("/users/:userId", userController.updateUser);
router.get("/users/:userId", userController.getUserById);
router.delete("/users/:userId", userController.deleteUser);

module.exports = router;
